# Changelog

All notable changes to [ngpr_visualiseur_core_print](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print) project will be documented in this file.

## [4.4.6](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.4.5...4.4.6) - 2024-12-06

## [4.4.5](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.4.4...4.4.5) - 2024-12-05

## [4.4.4](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.4.3...4.4.4) - 2024-12-04

## [4.4.3](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.4.2...4.4.3) - 2024-12-04

## [4.4.2](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.4.1...4.4.2) - 2024-10-22

## [4.4.1](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.3.25...4.4.1) - 2023-04-12

## [4.3.25](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.3.24...4.3.25) - 2022-11-08

## [4.3.24](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.3.23...4.3.24) - 2022-11-04

## [4.3.23](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.3.22...4.3.23) - 2022-11-04

## [4.3.22](https://gitlab.adullact.net/prodige/ngpr_visualiseur_core_print/compare/4.3.21...4.3.22) - 2022-11-04

