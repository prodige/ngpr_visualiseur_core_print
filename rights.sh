#!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`

if [ $(getent passwd "ansible-dev") ]; then
  OWNER="ansible-dev"
else
  if [ $(getent passwd "ftpuser") ]; then
    OWNER="ftpuser"
  else
    OWNER="www-data"
  fi
fi
APACHE="www-data"

SITEDIR=""
if [ -d "${SCRIPTDIR}/site" ]; then
  SITEDIR="/site"
fi

rights()
{
    if [ -d "${1}" ]
    then
        echo "Chowning ${1}"
        find "${1}" -print0 | xargs -0 chown ${2}.${3}
        if [ "${4}" == "ro" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 640
        fi
        if [ "${4}" == "rx" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 750
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 750
        fi
        if [ "${4}" == "rw" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 660
        fi
        if [ "${4}" == "rwx" ]; then
            echo "Chmoding ${1} directories ${4}"
            find "${1}" -type d -print0 | xargs -0 chmod 770
            echo "Chmoding ${1} files ${4}"
            find "${1}" -type f -print0 | xargs -0 chmod 770
        fi
    fi
}

chown ${OWNER}.${APACHE} "${SCRIPTDIR}${SITEDIR}"

rights "${SCRIPTDIR}${SITEDIR}" "${OWNER}" "${APACHE}" "ro"

chmod 750 "${SCRIPTDIR}${SITEDIR}"

find "${SCRIPTDIR}" -maxdepth 1 -name "*.sh" -type f -exec chown "${OWNER}".root {} \;
find "${SCRIPTDIR}" -maxdepth 1 -name "*.sh" -type f -exec chmod 750 {} \;

find "${SCRIPTDIR}${SITEDIR}" -maxdepth 1 -name "*.sh" -type f -exec chown "${OWNER}".root {} \;
find "${SCRIPTDIR}${SITEDIR}" -maxdepth 1 -name "*.sh" -type f -exec chmod 750 {} \;
