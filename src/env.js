(function (window) {
  window.__env = window.__env || {};

  /**
   * Ce fichier permet de surcharger les données d'environement du fichier env.service.
   * Décommenter uniquement ce qui doit être surchargé.
   */

  // window.__env.serverUrl = 'https://carto.prodige.internal/';
  window.__env.serverUrl = 'https://carto.geoguyane.fr/';
  window.__env.scaleList = [ 1, 2500, 5000, 10000, 18100, 36100, 50000, 100000, 250000, 500000, 1000000, 2000000, 4000000, 10000000 ];
  window.__env.isPrintMode = true;
  // window.__env.prodige41Url = 'https://prodige41.alkante.al:38001/';
  // window.__env.frontCartoUrl = 'https://prodige41.alkante.al:38001/';

  // window.__env.interrogationPointPrecision = 5;
  // window.__env.interrogationTooltipPrecision = 5;
  // window.__env.defaultMousePositionProjection = 'EPSG:4326';
  /* window.__env.availableProjections = {
    4326: {
      key: 'EPSG:4326',
      label: 'WGS84',
      proj: '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs',
      bounds: [-180.0000, -90.0000, 180.0000, 90.0000]
    }, ...
  }; */
}(this));
