import { Component, NgZone, OnInit } from '@angular/core';
import { OnMount } from '../dynamic-html/dynamic-html.interfaces';
import { FeatureCollection } from '@alkante/visualiseur-core/lib/models/context';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { EnvService } from '../env.service';
import { ChartOptionService } from '../services/chart-option.service';
import { lastValueFrom } from 'rxjs';
import { ImpressionView } from '@alkante/visualiseur-core/lib/models/option-impression';
import { transformExtent, transform } from 'ol/proj';
import { DrawLayerService, MapService, VisualiseurCoreService } from '@alkante/visualiseur-core';


@Component({
  selector:    `alk-build-olmap`,
  templateUrl: `./build-olmap.component.html`,
  styleUrls:   [`./build-olmap.component.scss`],
})
export class BuildOlmapComponent implements OnInit, OnMount {

  public canShow = false;

  public mapId = `visualiseur`;

  public templatePath = ``;
  public contextPath = ``;
  public tools: FeatureCollection['properties']['extension']['Tools'];

  private view: ImpressionView = null;

  private fields: {[key: string]: unknown};

  constructor(
    private http: HttpClient,
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private route: ActivatedRoute,
    private zone: NgZone,
    private environment: EnvService,
    private chartOptionService: ChartOptionService,
    public drawLayerService: DrawLayerService,
  ) {
  }

  ngOnInit(): void {
    this.mapService.getMapAfterComplete( this.mapId ).subscribe(( map ) => {

      this.mapService.verifyLayerGroupVisibility( this.mapService.rootLayerGroup, this.mapId );

      if ( this.view?.center ) {
        const point = this.view.projection ? transform( this.view.center, this.view.projection, map.getView().getProjection()) : this.view.center;
        map.getView().setCenter( point );
        map.getView().setZoom( this.view.zoom ? this.view.zoom : 12 );
      }

      if ( this.view?.projection && this.fields.bbox_100m && this.fields.bbox_100m instanceof Array && this.fields.bbox_100m.length === 4 ){
        const extentTmp =  [
          this.fields.bbox_100m[0],
          this.fields.bbox_100m[2],
          this.fields.bbox_100m[1],
          this.fields.bbox_100m[3],
        ];

        const extentTmp2 = transformExtent( extentTmp, this.view.projection, map.getView().getProjection());

        map.getView().fit( extentTmp2 );
      }
    });
  }

  async dynamicOnMount( attrs?: Map<string, string>, content?: string, element?: Element ): Promise<void> {
    this.canShow = true;
    await this.loadOptionFile();
  }

  private async loadOptionFile(){
    await lastValueFrom( this.chartOptionService.getOptionImpression$())
      .then(( optionImpression ) => {
        this.view = optionImpression?.view;
        this.fields = optionImpression?.fields;

      })
      .catch( console.error );
  }

}
