import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildOlmapComponent } from './build-olmap.component';

describe('BuildOlmapComponent', () => {
  let component: BuildOlmapComponent;
  let fixture: ComponentFixture<BuildOlmapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuildOlmapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildOlmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
