import { EnvService } from './env.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { VisualiseurCoreService, MapService, DrawLayerService } from '@alkante/visualiseur-core';
import { FeatureCollection } from '@alkante/visualiseur-core/lib/models/context';

import {ChartOptionService} from './services/chart-option.service';
import {firstValueFrom} from 'rxjs';
import {MapImpressionService} from './services/map-impression.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public mapId = 'visualiseur';
  public context: Promise<FeatureCollection> | null = null;
  public templateContent = '';

  public validParams = false;

  public templatePath = '';
  public contextPath = '';
  public tools: FeatureCollection['properties']['extension']['Tools'];

  constructor(
    private http: HttpClient,
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private route: ActivatedRoute,
    private zone: NgZone,
    private environment: EnvService,
    public drawLayerService: DrawLayerService,
    private chartOptionService: ChartOptionService,
    private mapImpressionService: MapImpressionService
  ) {


    this.route.queryParams.subscribe(params => {

      // On attend la liste des paramètres
      const paramsList = Object.keys(params);
      // On controle que les paramètres obligatoires sont présents :
      // contextPath
      // templatePath
      if (paramsList && paramsList.length >= 2) {
        this.zone.run(() => {
          this.validParams = true;
        });


        this.chartOptionService.loadOptionsData(params);

        if (params.templatePath && params.contextPath) {
          if (!this.contextPath && !this.templatePath) {
            this.contextPath = params.contextPath;
            this.templatePath = params.templatePath;

            const splittededContextPath = this.contextPath.split('/');
            const account = splittededContextPath.shift().toString();

            // Il nous faut d'abord récupérer le template à imprimer.
            firstValueFrom(this.http.get(this.environment.serverUrl + 'core/print/getTemplate/' + this.templatePath + '?account='+ account))
            .then((templateHtml: { template: string}) => {
              if (
                templateHtml &&
                templateHtml.template
              ) {
                templateHtml.template = templateHtml.template.replace(/(\r\n|\n|\r)/gm, '');
                this.templateContent = templateHtml.template;

                this.mapImpressionService.loadMap(this.contextPath);

                this.mapService.setSideBarLegendMode('Légende');
                this.coreService.isApplicationInLiteMode = true;


              }
            })
            .catch((errorGetTemplate: Error) => {
              console.error(errorGetTemplate);
            });
          }
        }
      }
    });
  }

  ngOnInit() {
    this.mapService.setMapId('visualiseur', this.mapId || 'main');
  }
}
