import { Component, OnInit } from '@angular/core';
import {OnMount} from '../dynamic-html/dynamic-html.interfaces';

@Component({
  selector: 'alk-legend-lite-builder',
  templateUrl: './legend-lite-builder.component.html',
  styleUrls: ['./legend-lite-builder.component.scss'],
})
export class LegendLiteBuilderComponent implements OnInit, OnMount {
  public canShow = false;

  constructor(

  ) {}

  ngOnInit(): void {

  }

  dynamicOnMount(attrs?: Map<string, string>, content?: string, element?: Element): void {
  }

}
