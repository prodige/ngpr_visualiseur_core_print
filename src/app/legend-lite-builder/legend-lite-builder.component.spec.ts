import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegendLiteBuilderComponent } from './legend-lite-builder.component';

describe('LegendLiteBuilderComponent', () => {
  let component: LegendLiteBuilderComponent;
  let fixture: ComponentFixture<LegendLiteBuilderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LegendLiteBuilderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegendLiteBuilderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
