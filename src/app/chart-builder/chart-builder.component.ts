import { Component, OnInit } from '@angular/core';
import {OnMount} from '../dynamic-html/dynamic-html.interfaces';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {lastValueFrom} from 'rxjs';
import {EnvService} from '../env.service';
import {ChartOptionService} from '../services/chart-option.service';
import {OptionChart, OptionImpression} from '@alkante/visualiseur-core';

@Component({
  selector: 'alk-chart-builder',
  templateUrl: './chart-builder.component.html',
  styleUrls: ['./chart-builder.component.scss']
})
export class ChartBuilderComponent implements OnInit, OnMount {
  public optionChart: OptionChart;

  public chartData: {[key: string]: unknown}|unknown;

  public showChart = false;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private environement: EnvService,
    private chartOptionService: ChartOptionService
  ) { }

  ngOnInit(): void {}

  public dynamicOnMount(attrs?: Map<string, string>, content?: string, element?: Element): void {

    if(attrs.get('template-id')){

      lastValueFrom(this.chartOptionService.getOptionImpression$())
        .then((optionImpression) => {

          this.getOptionsData(attrs.get('template-id'), optionImpression);
        })
        .catch(console.error);
    }
  }

  private getOptionsData(templateId: string, optionImpression: OptionImpression): void{
    let field = null;

    Object.entries(optionImpression.graph).forEach(([key, value]) => {
      if(value.templateId === templateId){
        this.optionChart = value;
        field = key;
      }
    });

    this.chartData = field && optionImpression.fields[field] ? optionImpression.fields[field] : null;

    this.showChart = !!(this.chartData && this.optionChart);


  }

}
