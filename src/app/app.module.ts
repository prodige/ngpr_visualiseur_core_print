import { EnvServiceProvider } from './env.service.provider';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { DragulaService, DragulaModule } from 'ng2-dragula';
import { DynamicHTMLModule } from './dynamic-html/dynamic-html.module';


import {
  LogoComponent,
  CopyrightComponent,
  SidebarComponent,
  MapVisualisationComponent,
  ScaleLineComponent,
  MapScaleDropdownComponent,
  InformationSheetFilterPipe,
  NumberToLabelPipe, VisualiseurCoreModule, MapModule,
  LegendFilterPipe
} from '@alkante/visualiseur-core';

import { ChartBuilderComponent } from './chart-builder/chart-builder.component';
import { DisplayFieldComponent } from './display-field/display-field.component';
import { BuildOlmapComponent } from './build-olmap/build-olmap.component';
import { LegendLiteBuilderComponent } from './legend-lite-builder/legend-lite-builder.component';

@NgModule({
  declarations: [
    AppComponent,
    ChartBuilderComponent,
    DisplayFieldComponent,
    BuildOlmapComponent,
    LegendLiteBuilderComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragulaModule,
    VisualiseurCoreModule,
    MapModule,
    DynamicHTMLModule.forRoot({
      components: [
        {component: SidebarComponent, selector: 'alk-sidebar'},
        {component: LogoComponent, selector: 'alk-logo'},
        {component: CopyrightComponent, selector: 'alk-copyright'},
        {component: MapVisualisationComponent, selector: 'alk-map-visualisation'},
        {component: ScaleLineComponent, selector: 'alk-controls-scale-line'},
        {component: MapScaleDropdownComponent, selector: 'alk-map-scale-dropdown'},
        {component: ChartBuilderComponent, selector: 'alk-chart-builder'},
        {component: DisplayFieldComponent, selector: 'alk-display-field'},
        {component: BuildOlmapComponent, selector: 'alk-build-olmap'},
        {component: LegendLiteBuilderComponent, selector: 'alk-legend-lite-builder'}
      ]
    })
  ],
  providers: [
    DragulaService,
    EnvServiceProvider,
    InformationSheetFilterPipe,
    NumberToLabelPipe,
    LegendFilterPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
