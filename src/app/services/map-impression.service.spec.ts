import { TestBed } from '@angular/core/testing';

import { MapImpressionService } from './map-impression.service';

describe('MapServiceService', () => {
  let service: MapImpressionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapImpressionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
