import {Injectable, NgZone} from '@angular/core';
import {FeatureCollection} from '@alkante/visualiseur-core/lib/models/context';
import {
  DrawLayerService,
  FontAwesomeSymbolDefinitionService,
  MapService,
  VisualiseurCoreService
} from '@alkante/visualiseur-core';
import {Fill, Icon, Stroke, Style} from 'ol/style';
import CircleStyle from 'ol/style/Circle';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';
import {EnvService} from '../env.service';
import FontSymbol from 'ol-ext/style/FontSymbol';
import FillPattern from 'ol-ext/style/FillPattern';
import StrokePattern from 'ol-ext/style/StrokePattern';
import {firstValueFrom} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MapImpressionService {
  public mapId = 'visualiseur';
  public context: Promise<FeatureCollection> | null = null;
  public templateContent = '';

  public validParams = false;

  public templatePath = '';
  public contextPath = '';
  public tools: FeatureCollection['properties']['extension']['Tools'];

  constructor(
    private http: HttpClient,
    private coreService: VisualiseurCoreService,
    private mapService: MapService,
    private route: ActivatedRoute,
    private zone: NgZone,
    private environment: EnvService,
    public drawLayerService: DrawLayerService,
  ) {
  }

  public loadMap(contextPathParam: string) {
    // On récupère les différentes paramètres de la reqûete.
    const splittededContextPath = contextPathParam.split('/');

    // On vérifie que le contexte semble être au bon format.
    if (splittededContextPath.length >= 2) {

      const account = splittededContextPath.shift().toString();
      const contextPath = splittededContextPath.join('/');

      const urlGetContext = this.environment.serverUrl + 'carto/context?account=' + account + '&contextPath=/' + contextPath;

      firstValueFrom(this.http.get<FeatureCollection>(urlGetContext))
        .then((context) => {
          if (['blue', 'grey', 'red', 'default'].includes(context.properties.extension.Layout.Theme)) {
            document.body.classList.add(context.properties.extension.Layout.Theme);
          } else {
            document.body.classList.add('default');
          }
          this.setDefaultAnnotationStyle();
          this.mapService.setFeatureTools(context.properties.extension.Tools);
          this.tools = context.properties.extension.Tools;
          this.coreService.setContext(context);
        })
        .catch((errorGetContext) => {
          console.error(errorGetContext);
        });
    } else {
      // ContextPath non valide.
      this.contextPath = '';
      this.templatePath = '';
    }
  }


  private setDefaultAnnotationStyle(): void {

    // tslint:disable-next-line: no-unused-expression
    new FontAwesomeSymbolDefinitionService();

    // Defaut definition of annotating's tool point
    const symbol = {
      glyph: 'fa-circle',
      fontSize: '0.3',
      fontStyle: 'normal',
      color: 'rgba(1, 149, 254, 0.85)',
      fill: new Fill({
        color: 'rgba(1, 149, 254, 0.85)'
      }),
      stroke: new Stroke({
        color: 'rgba(1, 149, 254, 0.85)',
        width: 2
      }),
      radius: 20
    };
    const defaultPoint = new Style({
      image: new FontSymbol(symbol)
    });
    this.drawLayerService.setPointStyle(defaultPoint);

    let defaultPattern = null;
    const emptyPattern = {
      empty: {
        width: 10,
        height: 10,
        lines: [],
        stroke: 0,
        fill: true
      }
    };
    FillPattern.prototype.patterns = Object.assign(emptyPattern, FillPattern.prototype.patterns);
    for (const [key, value] of Object.entries(FillPattern.prototype.patterns)) {
      if (key === 'empty') {
        defaultPattern = {
          name: key,
          data: value,
          pattern: new FillPattern({pattern: key}).getImage().toDataURL()
        };
      }
    }

    // Defaut definition of annotating's tool linestring
    const defaultLineString = new Style({
      stroke: new StrokePattern({
        width: 3,
        pattern: 'empty',
        ratio: 2,
        color: 'rgba(1, 149, 254, 0.85)',
        offset: 0,
        scale: 0,
        fill: new Fill({
          color: 'rgba(1, 149, 254, 0.85)'
        }),
        size: 7,
        spacing: 10,
        angle: 0
      })
    });
    this.drawLayerService.setLineStringStyle(defaultLineString);
    this.drawLayerService.setLineStringStyleOptions({
      settedStyle: {
        size: 7,
        color: 'rgba(1, 149, 254, 0.85)',
        background: 'rgba(1, 149, 254, 0.85)',
        pattern: defaultPattern,
        text: null
      }
    });

    // Defaut definition of annotating's tool polygon
    const defaultPolygon =
      new Style({
        fill: new FillPattern({
          pattern: 'empty',
          ratio: 2,
          icon: new Icon({
            src: 'data/target.png'
          }),
          color: 'rgba(1, 149, 254, 0.85)',
          offset: 0,
          scale: 0,
          fill: new Fill({
            color: 'rgba(0, 0, 0, 0.35)'
          }),
          size: 7,
          spacing: 10,
          angle: 0
        }),
        stroke: new Stroke({
          color: 'rgba(1, 149, 254, 0.85)',
          width: 3
        })
      });
    this.drawLayerService.setPolygonStyle(defaultPolygon);
    this.drawLayerService.setPolygonStyleOptions({
      settedStyle: {
        size: 7,
        color: 'rgba(1, 149, 254, 0.85)',
        background: 'rgba(0, 0, 0, 0.35)',
        pattern: defaultPattern,
        text: null
      }
    });

    // style par défault pour la selection et le tracé
    const fill = new Fill({
      color: 'rgba(1, 149, 254, 0.85)'
    });
    const stroke = new Stroke({
      // color: '#3399CC',
      color: 'rgba(0, 0, 0, 0.85)',
      width: 3.25
    });
    const defaultStyle =
      new Style({
        image: new CircleStyle({
          fill,
          stroke,
          radius: 5
        }),
        fill,
        stroke
      })
    ;
    this.drawLayerService.setDefaultDrawStyle(defaultStyle);
    this.drawLayerService.setDefaultSelectStyle(defaultStyle);
  }
}
