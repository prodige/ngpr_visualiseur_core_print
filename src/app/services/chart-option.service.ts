import { Injectable } from '@angular/core';
import {AsyncSubject, BehaviorSubject, Observable, switchMap, take} from 'rxjs';
import {ActivatedRoute, Params} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {EnvService} from '../env.service';
import {OptionImpression} from '@alkante/visualiseur-core';

@Injectable({
  providedIn: 'root'
})
export class ChartOptionService {

  private options$ = new AsyncSubject<OptionImpression>();

  constructor(
    private http: HttpClient,
    private environement: EnvService
  ) { }

  /** */
  public loadOptionsData(params: Params): boolean {
    if(!params.optionsPath){
      this.options$.next(null);
      this.options$.complete();
      return false;
    }

    const contextPath = params.contextPath;
    const optionsPath = params.optionsPath;

    const splittededContextPath = contextPath.split('/');
    const account = splittededContextPath.shift().toString();

    this.http.get<OptionImpression>(this.environement.serverUrl + 'core/print/getOptions/' + optionsPath + '?account=' + account).pipe(take(1))
    .subscribe(result => {
      this.options$.next(result);
      this.options$.complete();
    });

    return true;
  }

  /** */
  public getOptionImpression$(): Observable<OptionImpression>{
    return this.options$;
  }



}
