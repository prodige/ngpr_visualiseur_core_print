import {Component, OnInit} from '@angular/core';
import {OnMount} from '../dynamic-html/dynamic-html.interfaces';
import {lastValueFrom} from 'rxjs';
import {ChartOptionService} from '../services/chart-option.service';
import {InformationSheetFilterPipe, NumberToLabelPipe} from '@alkante/visualiseur-core';

@Component({
  selector: 'alk-display-field',
  templateUrl: './display-field.component.html',
  styleUrls: ['./display-field.component.scss']
})
export class DisplayFieldComponent implements OnInit, OnMount {
  public fieldValue;

  public showField = false;

  constructor(
    private chartOptionService: ChartOptionService,
    private informationSheetPipe: InformationSheetFilterPipe,
    private numberToLabel: NumberToLabelPipe
  ) { }

  ngOnInit(): void {
  }

  dynamicOnMount(attrs?: Map<string, string>, content?: string, element?: Element): void {
    const field = attrs.get('field');
    const applyPipe = attrs.get('apply-pipe');

    if(field){
      lastValueFrom(this.chartOptionService.getOptionImpression$())
        .then((optionImpression) => {
          this.fieldValue = optionImpression.fields[field];

          if(applyPipe){
            this.fieldValue = this.execPipe(applyPipe, this.fieldValue, attrs);
          }

          this.showField = true;

        })
        .catch(console.error);
    }
  }

   private execPipe(pipeStr: string, field: string|number, attribut: Map<string, string>): string|number{
      const pipeList = pipeStr.split(',');
      pipeList.forEach(pipeName => {
        switch (pipeName){
          case 'informationSheetFilter':
            const digit = parseInt(attribut.get('digit'), 10);

            if(digit || digit === 0){
              field = this.informationSheetPipe.transform(field, digit);
            } else{
              field = this.informationSheetPipe.transform(field);
            }
          break;
          case 'numberToLabel':
            const operator = attribut.get('operator') as ('<' | '>' | '<=' | '>=' | '===');
            const value2 =  parseFloat(attribut.get('value2'));
            const label = attribut.get('label');
            field = this.numberToLabel.transform(field,operator,value2, label);
          break;
          default:
            console.error(pipeName, `can't transform`);
        }
      });

      return field;
  }

}
