# NgprProdigeImpression

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.
To install modules (especially visualiseur-core), you need to :

1) For credentials :
`git config --global credential.helper cache`

2) Run this command, and enter your username/password :
`/usr/bin/git ls-remote -h -t https://gitlab.alkante.com/angular/bdl-alkante-visualiseur-core`

3) Then :
`npm install`

# Environment
To update environment in builded project, please use this following file :
`./dist/env.js`

Simply add or uncomment : 
`window.__env.XXX = YYY;` ;

With:
- XXX : An attribute from env.service.
- YYY : Desired value.

No need to build again. The environment will be update during the refresh of the app webpage.

## Development

See ```./cicd/dev/README.md

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
